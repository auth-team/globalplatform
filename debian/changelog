globalplatform (2.4.0+dfsg-3) unstable; urgency=medium

  * Fix reprotest failure.
  * Fix gpshell.1 paths.  Closes: #1079704.
  * Run wrap-and-sort -satbk.

 -- Simon Josefsson <simon@josefsson.org>  Mon, 23 Sep 2024 22:32:50 +0200

globalplatform (2.4.0+dfsg-2) unstable; urgency=medium

  [ Simon Josefsson ]
  * Bump Standards-Version to 4.7.0.
  * Add debian/salsa-ci.yml for wrap-and-sort.
  * Change Maintainer to pkg-security and adjust Vcs-* URLs.
  * Build-Depends: Use pkgconf instead of pkg-config.
  * Bump d/copyright years.
  * d/gbp.conf: Add, for git branch names.

  [ Sudip Mukherjee <sudipm.mukherjee@gmail.com> ]
  * d/p/fix-ftbfs.patch: Disable LTO in unittests. (LP: #2060262)

 -- Simon Josefsson <simon@josefsson.org>  Thu, 04 Jul 2024 09:49:46 +0200

globalplatform (2.4.0+dfsg-1) unstable; urgency=medium

  * New upstream release.
  * use-MAJOR-version-as-SOVERSION.patch: Drop, fixed upstream.
  * fix-unitialized-value.patch: Drop, fixed upstream.
  * fix-rsa-exponent-conversion.patch: Drop, fixed upstream.
  * Update symbols file with added APIs.
  * use-versioned-so-for-dlopen.patch: Mark patch as forwarded.
  * use-system-minizip.patch: Tag Forwarded:not-needed with explanation.

 -- Simon Josefsson <simon@josefsson.org>  Tue, 19 Dec 2023 11:07:31 +0100

globalplatform (2.3.1+dfsg-6) unstable; urgency=medium

  [ Ludovic Rousseau ]
  * Fix autopkgtest problem with pcscd. Closes: #1058770

 -- Simon Josefsson <simon@josefsson.org>  Sun, 17 Dec 2023 09:20:18 +0100

globalplatform (2.3.1+dfsg-5) unstable; urgency=medium

  * fix-rsa-exponent-conversion.patch: Add; fixing s390x FTBFS.

 -- Simon Josefsson <simon@josefsson.org>  Fri, 15 Dec 2023 09:28:48 +0100

globalplatform (2.3.1+dfsg-4) unstable; urgency=medium

  * Mark libglobalplatform-dev as Multi-Arch:same.
  * fix-unitialized-value.patch: Add; re-enable self-tests on all archs.

 -- Simon Josefsson <simon@josefsson.org>  Mon, 20 Nov 2023 22:42:58 +0100

globalplatform (2.3.1+dfsg-3) unstable; urgency=medium

  * Add Multi-Arch hints.
  * Ship example *.txt scripts in gpshell package.
  * Add upstream/metadata.
  * tests: Add 'lib' test of libraries.
  * tests: Add 'cli' test of gpshell.
  * libglobalplatform-dev: Depend on libpcsclite-dev.

 -- Simon Josefsson <simon@josefsson.org>  Wed, 15 Nov 2023 08:44:51 +0100

globalplatform (2.3.1+dfsg-2) unstable; urgency=low

  * Drop self-checks on some archs due to upstream bug.

 -- Simon Josefsson <simon@josefsson.org>  Tue, 14 Nov 2023 08:15:11 +0100

globalplatform (2.3.1+dfsg-1) unstable; urgency=low

  * Initial release.  Closes: #656665, #656959, #656957.

 -- Simon Josefsson <simon@josefsson.org>  Mon, 09 Oct 2023 10:17:09 +0200
